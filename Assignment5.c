#include<stdio.h>
/*This program will identify the relationship between profit and ticket price 
so that the owner can determine the price at which he can make the highest profit*/

//calculating income
int income(int price,int cusm);
//calculating expenditure
int expenditure(int cusm);
//calculating profit
int profit(int income,int expenditure);
//calculating no of attendees 
int cusm(int price);
//showing profit according to ticket price
void ticketProfit(); 

int income(int price,int cusm){
	return price*cusm;	
}

int expenditure(int cusm){
	return 500+(3*cusm);
}

int profit(int income,int expenditure){
	return income-expenditure;	
}

int cusm(int price){
	int difference=15-price;
	return 120+(difference/5*20);	
}

void ticketProfit(int profit,int price){
	printf("%d    \t %d     \n",price,profit);
}


int main(){
	int t,p,n; //t= ticket price , n= no of customers , p= profit
	int max=0,maxt=0;//max=maximum profit , maxt=ticket price of maximum profit
	int maxn=0;
	printf("price \t profit \n");
	for(t=10;t<=40;t+=5)//profit is positive only in range 10<= t <=40
	{
		n=cusm(t);
		p=profit(income(n,t),expenditure(n));
		ticketProfit(p,t);
		if(p>max)
		{
			max=(int)p;
			maxt=t;
			maxn=n;
		}
	}
	printf("\n");
	printf("maximum profit = %d\n",max);
	printf("ticket price   = %d\n",maxt);
	return 0;
}


