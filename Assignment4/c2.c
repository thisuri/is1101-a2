#include<stdio.h>
/*find the volume of the cone where the user inputs the height and
radius of the cone*/

int main()
{
	float radius,height,volume=0;
	const double PI=3.14;
	
	printf("Enter the height \n");
	scanf("%f",&height);
	printf("Enter the radius \n");
	scanf("%f",&radius);
	
	printf("\nheight = %f \t radius = %f \n",height,radius);
	
	volume=PI*radius*radius*(height/3);
	
	printf("volume of the cone = %f \n",volume);
	  
	return 0;
}

