#include<stdio.h>
/*This program will calculate the sum and average of 
any given 3 integers and print the output */

int main()
{
	int n1,n2,n3,tot=0;
	float ave=0;
	
	printf("Enter 3 integers \n");
	scanf("%d %d %d",&n1,&n2,&n3);
	
	printf("number 1= %d \nnumber 2= %d \nnumber 3= %d \n",n1,n2,n3);
	
	tot=n1+n2+n3; //tot=total
	ave=(float)tot/3;    //ave=average
	
	printf("total = %d\n",tot);
	printf("average = %f \n",ave);
	 
	return 0;
}

