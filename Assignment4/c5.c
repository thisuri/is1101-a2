#include<stdio.h>
/*This program will demonstrate given bitwise operators  */
int printb(int);

int main()
{
	int A=10,B=15,C,D,E,F,G;
	
	printf("A = %d B = %d\n",A,B);
	printf("A = %d B = %d\n\n",printb(A),printb(B));
	C=A&B;
	printf("%d & %d = %d\n",A,B,C);
	printf("A & B = %d\n\n",printb(C));
	D=A^B;
	printf("%d ^ %d = %d\n",A,B,D);
	printf("A ^ B = %d\n\n",printb(D));
	E=~A;
	printf("~ %d = %d\n",A,E);
	printf("~ A = %d\n\n",printb(E));
	F=A<<3;
	printf("%d << 3 = %d\n",A,F);
	printf("A << 3 = %d\n\n",printb(F));
	G=B>>3;
	printf("%d >> 3 = %d\n",A,G);
	printf("B >> 3 = %d\n\n",printb(G));
	return 0;
}

int printb(int n)
{
	if(n==0) return 0;
	else return (n%2) + 10*printb(n/2);
}

